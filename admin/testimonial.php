<?php require_once('header.php'); ?>

<section class="content-header">
	<div class="content-header-left">
		<h1><?php echo LANG_VALUE_324; ?></h1>
	</div>
	<div class="content-header-right">
		<!-- <a href="testimonial-add.php" class="btn btn-primary btn-sm">Add Testimonial</a> -->
	</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info">
				<div class="box-body table-responsive">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="30"><?php echo LANG_VALUE_309; ?></th>
								<th><?php echo LANG_VALUE_310; ?></th>
								<th width="100"><?php echo LANG_VALUE_325; ?></th>
								<th width="100"><?php echo LANG_VALUE_326; ?></th>
								<th width="100"><?php echo LANG_VALUE_327; ?></th>
								<th><?php echo LANG_VALUE_328; ?></th>
								<th><?php echo LANG_VALUE_329; ?></th>
								<th width="80"><?php echo LANG_VALUE_319; ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i=0;
							$statement = $pdo->prepare("SELECT
														
														id,
														name,
														designation,
														company,
														photo,
														comment,
														comment_chinese

							                           	FROM tbl_testimonial
							                           	
							                           	");
							$statement->execute();
							$result = $statement->fetchAll(PDO::FETCH_ASSOC);							
							foreach ($result as $row) {
								$i++;
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<td style="width:130px;"><img src="../assets/uploads/<?php echo $row['photo']; ?>" alt="<?php echo $row['name']; ?>" style="width:120px;"></td>
									<td><?php echo $row['name']; ?></td>
									<td><?php echo $row['designation']; ?></td>
									<td><?php echo $row['company']; ?></td>
									<td><?php echo $row['comment']; ?></td>
									<td><?php echo $row['comment_chinese']; ?></td>
									<td>										
										<a href="testimonial-edit.php?id=<?php echo $row['id']; ?>" class="btn btn-primary btn-xs"><?php echo LANG_VALUE_320; ?></a>
										<!-- <a href="#" class="btn btn-danger btn-xs" data-href="testimonial-delete.php?id=<?php echo $row['id']; ?>" data-toggle="modal" data-target="#confirm-delete">Delete</a>   -->
									</td>
								</tr>
								<?php
							}
							?>							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


</section>


<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Delete Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure want to delete this item?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>


<?php require_once('footer.php'); ?>